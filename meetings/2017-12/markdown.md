
class: center, middle

# December 2017

---

# Agenda

1. Engagement results
1. Christmas

---

# Engagement

--
- 1 to 6
    - Strongly disagree (1), disagree (2), somewhat disagree (3)
    - Somewhat agree (4), agree (5), strongly agree (6)
--
- Team: 4.6

--
- Solutions average: 4.7
---

# Engagement - the good
--

- I would like to be working at this organisation one year from today (5.6)

--
- I know I can depend on the other members of my team (5.4)

--
- The people I work with most closely are committed to producing top quality
  work (5.4)

--
- I am always thinking about ways to do my job better (5.3)

--
- I enjoy doing my work (5.3)

---

# Engagement - the bad
--

- There is cooperation between my department and other departments we work with
  (3)

--
- The company's internal technologies allow me to do my best work (3.3)

--
- Our team developed action plans to address issues raised by the last survey's
  results (3.5)

--
- I see professional growth and career development opportunities for myself in
  this organization (3.5)

--
- I am paid fairly (3.6)

---

# Engagement - the ugly
--

- There is cooperation between my department and other departments we work with
  (-1.1)

--
- The company's internal technologies allow me to do my best work (-0.9)

--
- I have the materials and equipment I need to do my job (-0.8)

---

# Engagement - the cat
--

![cat](cat.webp)

---

# Engagement - the cat

- I enjoy doing my work (+0.4)

--
- I know I can depend on the other members of my team (+0.3)

--
- I would like to be working at this organisation one year from today (+0.3)

--
- I love the people I work with (+0.3)

---

# Engagement - themes

--
- Good: Engagement, team, empowerment, future outlook

--
- Poor: Leadership, growth and development, communication and resources,
  individual needs

---

# Engagement - next steps

--
- It's your engagement

--
- Action plan

--
- What do you think?

---

# Christmas festivity
