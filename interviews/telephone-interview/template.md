# Introduction

1. Overview of process



2. Availability



3. Based



# Questions


1. What is the `static` keyword?



2. What is the `final` keyword?



3. What are the different visibility in Java, public, private, etc.?



4. Why not make everything public?



5. How would you go about concatenating 100 strings together?



6. Describe some ways of achieving code reuse? Inheritance, composition, having
   an interface, patterns like 'strategy pattern'.



7. Engine class in codebase (start, stop, speed up, slow down). Need a Car class
   (start, stop, speed up, slow down).



8. What is dependency injection?



9. What do you have between the server application and the database?



10. What are the advantages and disadvantages of ORMs?



11. What is TDD?



12. What makes a good unit test?



13. What is the right level of code coverage?



14. Any questions?


