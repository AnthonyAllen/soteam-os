- Programming Exercise
  - What did you find most challenging about the programming exercise?
  - How did you approach the solution?
  - What would you do differently next time?
  - If you were developing this for yourself as a side-project, without any of the technology constraints we've specified, how would you do it?

- Why do you want to be a software developer? What are the best points and what are the worst?
(Possible Answer: No right answer, just food for thought.)

- Any developer should be able to write code that fulfils the requirements that have been set out for them. However, what qualities do you think “good” code should have? 
(Possible Answer: Readable, maintainable, concise, self-documenting rather that verbose commenting.)

- In your opinion, what do you think is the adequate level of test coverage. (Possible Answer: So long as they don’t say 100% as there are diminishing returns.)

- What do you understand the difference between Unit Testing and Integration Testing.
(Possible Answer: Unit Testing are for isolated segments of code and should make it easier to test all possible permutations. Integration Tests are more of an end-to-end test.)

- Which do you see as more useful, Unit Testing or Integration Testing?
(Possible Answer: They both have their place. Unit Tests can be more thourough testing of code segments. Integration Tests can provide more realistic tests, but it may take longer to setup and execute)

- How did you first get interested in computers? What was it that attracted you to them in the first place?

- What was your favourite subject / topic of study at university / college / school*? What was it about that subject that interested you most?

- What proportion of your time would you like to spend doing hands-on development, vs requirements/analysis/testing/documentation?

- What is your favourite thing about being a developer?

- What is the hardest thing you have had to do technically? How did it go?
