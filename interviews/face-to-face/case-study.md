# Case study

You are designing a web service for the central authority on all the lamp posts
in the UK. Each week every local authority will send a large upload with all the
current lamp posts in their authority. This will include the asset number of the
lamp post, coordinates of the lamp post, the current status of the lamp post
(active, day burning, non-functional, awaiting activation), the local authority
to which the lamp post belongs.

If a lamp post has been removed, it will not be included in the upload. There
will be no other indication. These lamp posts should be marked as removed in the
database.

The Central Authority wants to be able to report on all active lamps in the UK
as well as do spatial reporting upon groupings of lamp post.
