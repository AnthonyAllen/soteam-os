# Questions

A list of suggested questions to investigate *interest* and *engagement*:

- Programming Exercise
    - What did you find most challenging about the programming exercise?
    - How did you approach the solution?
    - What would you do differently next time?
    - If you were developing this for yourself as a side-project, without any of
      the technology constraints we've specified, how would you do it?
- Experience
    - Please talk us through the last two or three years of your CV - how did
      you get to where you are now?
    - How would you like to develop your career in the future?
- Lamp post case study
    - How have you gone about fleshing out requirements for something like this?
    - How have you gone about arranging the architecture/layers/modules for
      something like this?
    - How would you go about implementing the upload process that marks records
      as logically deleted?  Imagine receiving an upload of 9,000 records when
      there are 10,000 active records in the database.
    - After passing the build tests, the application is deployed to a test
      server, but after sending an upload you find the database is still empty.
      How would you go about diagnosing the problem?
    - A new requirement for handling traffic lights arrives.  How would you go
      about accommodating this within the design?
    - A colleague is assigned to make the traffic light changes to the software.
      You disagree with the way the changes were made. How do you go about
      discussing that?
- Background
    - What proportion of your time would you like to spend doing hands-on
      development, vs requirements/analysis/testing/documentation?
    - What is your favourite thing about being a developer?
    - What is the hardest thing you have had to do technically?  How did it go?
    - Have you raised things for improvement in the past?  Were they implemented?
