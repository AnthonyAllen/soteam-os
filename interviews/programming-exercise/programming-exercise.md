# Programming Exercise

The purpose of this exercise is for you to demonstrate how you would _begin_ to go
about implementing the requirements outlined below.  We do not expect a
complete, production quality solution to the entire problem.  Having said that,
we would like it to be treated as if it was the _beginnings_ of a complete
production quality solution.  I.e., although we might put everything into the
controller layer for a toy project, we would not expect that for a production
project.  We will be looking for evidence of your _working practices_ and
_approaches_.

You are tasked to write simple REST web services for an online grocery
wholesaler, data on products is added via JSON (see appendix).  The
functionality of the web services should comprise:

- An endpoint where the new JSON can be uploaded (removing all existing data
and replacing it with the contents of the upload).
- A retrieval service where data on all produce are returned with their prices
and stock levels, ordered by their update date.
- A service where the prices of produce can be updated
- A service function where produce can be searched for by name and results
returned.

You do not need to worry about implementing login/security as part of this
functionality.

You will be expected to write to a database as part of the functionality in
order to track the groceries.  Postgres, MySQL, MongoDB or CouchDB are all
possibilities.

You can use Java, NodeJS, Go or Python to implement the REST services.

The project must be submitted as a **zipped Git repository**.  The Git
repository must contain a commit history that gives us an indication of
the steps you have taken to approach the exercise.

# Appendix

```json
[{
    "name" : "banana",
    "price" : 0.29,
    "stock" : 20,
    "updated" : "2014-01-02"
}, {
    "name" : "melon",
    "price" : 1.01,
    "stock" : 3,
    "updated" : "2014-03-28"
}, {
    "name" : "apple",
    "price" : 1.54,
    "stock" : 22,
    "updated" : "2014-02-05"
}, {
    "name" : "pear",
    "price" : 0.41,
    "stock" : 12,
    "updated" : "2014-04-19"
}, {
    "name" : "kumquat",
    "price" : 0.64,
    "stock" : 32,
    "updated" : "2014-06-10"
}, {
    "name" : "orange",
    "price" : 2.04,
    "stock" : 19,
    "updated" : "2014-05-25"
}, {
    "name" : "lemon",
    "price" : 1.56,
    "stock" : 9,
    "updated" : "2014-12-30"
}]
```
