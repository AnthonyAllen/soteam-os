# Programming Exercise - Assessment Questions

The following questions can be used to assist in assessing the quality of
candidate responses to the [Programming Exercise](programming-exercise.md):

## Submission Format
- Has the exercise been supplied as a Git repo?
- Has a good commit history been supplied that indicates how the candidate has
  tackled the problem?

## Documentation
- Has a useful `README` file been supplied with instructions on how to set up
  and run the example?
- Has a suitable level of documentation provided within the code itself?

## Building & Running
- Is there an easy way to build and run the app server with the app (e.g. `mvn
  cargo:run`, `gradle bootRun` or equivalent)?
- Does the example app work when built and run?

## Tests
- Has the example been supplied with any tests?
- Does the example demonstrate an understanding of unit testing?
- Are both unit and end-to-end integration tests supplied?
- Does the `README` files document a clear way to build and run the different
  types of tests?

## Design
- Does the example show a good understanding of separation into layers (e.g.
  controller / service / data access)?
- Does the example show a good understanding of the use of common HTTP verbs
  (e.g. POST vs GET)?
- Is the example subject to any obvious security vulnerabilities (e.g. SQL
  injection)?
- Has the candidate made sensible judgements about datatypes for various
  properties?
- Has the transactional nature of deleting the database and adding new items
  been handled correctly?

## Miscellaneous
- Has the example been coded in a concise manner?
- Has the candidate demonstrated an innovative or novel approach, for example
  through choice of tools and technologies?
