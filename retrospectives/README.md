# Retrospectives Process

- A new file is created at the end of each sprint, named after the date of the
  retrospective meeting (e.g. `2019-01-23.md`).
- The file is headed with the name and number of the sprint ("Sprint 10") and
  has subheadings of "Liked", "Lacked" and "Learnt"
- Each subsection is a list of bullet points describing what went well and what
  should have been done better.
- A log of actions is maintained.  Each retrospective, open actions are reviewed
  and possibly closed.  New actions are appended.
- This file may be reviewed and amended in a retrospective.
- The new retrospective file, changes to the actions log and any changes to this
  file are committed to a sprint specific branch in the repository.  All
  retrospective attendees are added as peer review reviewers.
- Once any review comments are resolved, the retrospective branch can be merged
  to master.
