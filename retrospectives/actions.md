# Actions

- Fix FRE dev Jenkins/tests
- Try MS Teams
- Find opt-in-out CR
- Document training documentation in Sharepoint
- Another QA req - Ian
- Jenkins issues -> Kanban

- Have refinement/planning for EPC and FRE
- Add a loved column for the next retrospective
- Create a ticket for Sprint 29 to fix the EPC release test pack

- Review Steph and Chris's start-up documentation, get Chris to use Steph's and vice-versa.
- Monitor release date slippage (AM)
- Review the resource profile on FRE


- Walk the board in daily scrums
- Begin EPC refinement (AA)
- Progress towards a developer operated Flood Re Jenkins
- Do more EPC handover (JC/DK/ME)
- Check FRE release date, might be tight due to covering EPC QA

- Escape from the proxy (RoB and AA)
- Identify infrastructure change tickets in planning (All)
- Investigate claims identify bug  (RoB & DK)
- Reduce Mina's available time due to Met Office (All)
- When unsure about FRE reporting (show and tell gleaned) requirements, check
  with LW (DK)
- Feedback to Greg the difficulties of not being agile, e.g. where the claims
  cancellation work came closer, it looked more complex than it did at the BIA
  stage, where we were proposing three possibilities. The only way to uncover
  all of the uncertainties is to actually do it, but by the time we get to do
  it, we have already committed to estimates and dates made without those
  uncertainties being evident. (AA)

- Track actuals vs estimates
- Check Acceptance Criteria in refinement before labelling as 'refined'
- Avoid implementation details in functional specifications
- Ensure Jira tickets capture everything in functional specifications
- Start sprints with complete requirements
- Fix Cucumber/Selenium tests
- Review forward schedule of change in sprint planning
- Use the fix version...
- Don't over-allocate the sprint, be prepared to drag in new stuff
- Account for time taken by non-sprint tasks, e.g. auditors
- Have a reliable proxy service
- Try accounting for (and recording) availability vs estimates
- Add 1d to training tickets
- Can analysis be done to determine if the 75% allocation of time to the sprint is still valid – Anthony you got this action as you weren’t there….sorry
- Can we feed back to the team, how free resources should be allocated. For
  example on FloodRe we have one test resource, but we putting work in the
  sprint that requires extra resource, which means we are over delivering but also
  over allocating on Flood Re. Action on GG, AM, RF and AA to review and feedback
- Add in regression QA into tickets and impact assessments – note speak to Rich
  about this and as a team decide whether this impacts the releases, if it does
  it adds significant delays to our release schedule (FSC)
- Ensure going forward that even if work does not have a QA element that QA is
  still marked as a reviewer
- Talk to Ian about the estimates on the EPC tickets, James is over allocated
- Talk to Capita about extra testing resource and impact on deliverables
