# How to contribute

- Create a new branch
- Submit a pull request
- Add all members of a team as reviewers
- Deal with any comments, until all reviewers approve
- Approve the PR yourself
- Merge to master
